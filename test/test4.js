let expect = require("chai").expect;
let quest4 = require("../questions/quest4.js");

describe("test-quest4", function () {
    it("should return the economy of top economical bowlers of the ipl season 2015", async function () {
        let expectedResult = [{
                label: 'TS Mills',
                y: 0
            },
            {
                label: 'SB Jakati',
                y: 3
            }
        ]
        let result = await quest4.getTopEconomicalBowlers("ipltestdb");
        // console.log(result);
        expect(expectedResult).deep.equals(result);
    })
})