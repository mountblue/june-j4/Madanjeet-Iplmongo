let expect = require("chai").expect;
let quest2 = require("../questions/quest2.js");

describe("test-quest2", function () {
    it("should return number of matches played per team per season of ipl", async function () {
        let expectedResult = [{
                name: 'Kings XI Punjab',
                season: 2016,
                count: 1
            },
            {
                name: 'Kings XI Punjab',
                season: 2014,
                count: 1
            },
            {
                name: 'Kolkata Knight Riders',
                season: 2017,
                count: 1
            },
            {
                name: 'Royal Challengers Bangalore',
                season: 2016,
                count: 2
            },
            {
                name: 'Royal Challengers Bangalore',
                season: 2015,
                count: 1
            },
            {
                name: 'Royal Challengers Bangalore',
                season: 2017,
                count: 2
            },
            {
                name: 'Sunrisers Hyderabad',
                season: 2015,
                count: 1
            }
        ]

        let result = await quest2.getCountOfMatchesWonByEachTeam("ipltestdb");
        // console.log(result);
        expect(result).deep.equals(expectedResult);
    })
})