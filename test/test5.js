let expect = require("chai").expect;
let quest5 = require("../questions/quest5.js");

describe("test-quest5", function () {
    it("should return the maximum number of matches played per city of all seasons of ipl", async function () {
        let expectedResult = [{
                label: 'Hyderabad',
                y: 2
            },
            {
                label: 'Pune',
                y: 2
            },
            {
                label: 'Indore',
                y: 2
            },
            {
                label: 'Rajkot',
                y: 1
            },
            {
                label: 'Mumbai',
                y: 1
            }, {
                label: 'Bangalore',
                y: 1
            }
        ]
        let result = await quest5.getCountOfMatchesPerCity("ipltestdb");
        // console.log(result);
        expect(expectedResult).deep.equals(result);
    })
})