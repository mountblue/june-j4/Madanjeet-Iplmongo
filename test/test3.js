let expect = require("chai").expect;
let quest3 = require("../questions/quest3.js");

describe("test-quest3", function () {
    it("should return the extra runs conceded per team in the year 2016", async function () {
        let expectedResult = [{
            label: 'Gujarat Lions',
            y: 2
        }]
        let result = await quest3.getExtraRunsConcededPerTeam("ipltestdb");
        // console.log(result);
        expect(result).deep.equals(expectedResult);
    })
})