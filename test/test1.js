let expect = require("chai").expect;
let quest1 = require("../questions/quest1.js");

describe("test-quest1", function () {
    it("should return number of matches played in each season of ipl", async function () {
        let expectedResult = [{
                label: 2014,
                y: 1
            },
            {
                label: 2015,
                y: 2
            },
            {
                label: 2016,
                y: 3
            },
            {
                label: 2017,
                y: 3
            }
        ];
        result = await quest1.getCountOfMatchesPerYear("ipltestdb");
        // console.log(result);

        expect(result).deep.equal(expectedResult);
    })

})