const MongoClient = require("mongodb").MongoClient;

let url = "mongodb://127.0.0.1:27017";

function getCountOfMatchesWonByEachTeam(database) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, conn) {
            if (err) {
                console.log(err.message);
            } else {
                let ipldb = conn.db(database);
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.aggregate([{
                        "$group": {
                            "_id": {
                                "winner": "$winner",
                                "season": "$season"
                            },
                            "count": {
                                $sum: 1
                            }
                        }
                    },
                    {
                        "$sort": {
                            "_id.winner": 1
                        }
                    },
                    
                    {
                        $project:
                        {
                            name:"$_id.winner",
                            season:"$_id.season",
                            count:"$count",
                            _id:0
                        }
                    }
                ]).toArray(function (err, data) {
                    if (err) {
                        console.log(err.message);
                    } else {
                        // require("fs").writeFile("JSONfiles/quest2.json", JSON.stringify(data, null, 10), () => {
                        //     console.log("file created");
                        // })
                        resolve(data);
                    }
                })
                conn.close();
            }
        })
    })

}

// getCountOfMatchesWonByEachTeam("ipldb");

module.exports = {
    getCountOfMatchesWonByEachTeam: getCountOfMatchesWonByEachTeam
}