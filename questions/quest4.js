const MongoClient = require("mongodb").MongoClient;

let url = "mongodb://127.0.0.1:27017";

function getTopEconomicalBowlers(database) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, conn) {
            if (err) {
                console.log(err.message);
            } else {

                let ipldb = conn.db(database);
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.aggregate([{
                        "$match": {
                            season: 2015
                        }
                    },
                    {
                        $lookup: {
                            from: "deliveriesCol",
                            localField: "id",
                            foreignField: "match_id",
                            as: "deliver"
                        }
                    },
                    {
                        $unwind: "$deliver"
                    }, {
                        $group: {
                            _id: "$deliver.bowler",
                            totalRuns: {
                                $sum: "$deliver.total_runs"
                            },
                            totalBalls: {
                                $sum: {
                                    $cond: {
                                        if: {
                                            $gt: ["$deliver.wide_runs", 0]
                                        },
                                        then: 0,
                                        else: {
                                            $cond: {
                                                if: {
                                                    $gt: ["$deliver.noball_runs", 0]
                                                },
                                                then: 0,
                                                else: 1
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        $project: {
                            "economy": {
                                $divide: [{
                                    $multiply: ["$totalRuns", 6]
                                }, "$totalBalls"]
                            }
                        }
                    }, {
                        $sort: {
                            economy: 1
                        }
                    }, {
                        $limit: 10
                    },{
                        $project:
                        {
                            "_id":"$null",
                            "label":"$_id",
                            "y":"$economy"
                        }
                    }
                ]).toArray(function (err, data) {
                    if (err) {
                        console.log(err.message);
                    } else {
                        // require("fs").writeFile("JSONfiles/quest4.json", JSON.stringify(data, null, 10), () => {
                        //     console.log("file created");
                        // })
                        resolve(data);
                    }
                })
                conn.close();
            }

        })
    })
}
// getTopEconomicalBowlers("ipldb");

module.exports = {
    getTopEconomicalBowlers: getTopEconomicalBowlers
}