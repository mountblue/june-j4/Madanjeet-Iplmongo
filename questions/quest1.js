const MongoClient = require("mongodb").MongoClient;
let url = "mongodb://127.0.0.1:27017";

function getCountOfMatchesPerYear(database) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, conn) {
            if (err) {
                console.log(err.message);
                reject(err);
            } else {
                let ipldb = conn.db(database);
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.aggregate([{
                        "$group": {
                            "_id": "$season",
                            "count": {
                                $sum: 1
                            }
                        }
                    },
                    {
                        "$sort": {
                            "_id": 1
                        }
                    }, {
                        $project: {
                            _id: "$null",
                            label: "$_id",
                            y: "$count"
                        }
                    }
                ]).toArray(function (err, data) {
                    if (err) {
                        console.log(err.message);
                        reject(err);
                    } else {
                        // require("fs").writeFile("JSONfiles/quest1.json", JSON.stringify(data, null, 10), () => {
                        //     console.log("file created");
                        // })
                        resolve(data);
                    }

                })
                conn.close();
            }
        })
    })
}

// getCountOfMatchesPerYear("ipldb")

module.exports = {
    getCountOfMatchesPerYear: getCountOfMatchesPerYear
}