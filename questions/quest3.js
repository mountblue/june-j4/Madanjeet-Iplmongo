const MongoClient = require("mongodb").MongoClient;

let url = "mongodb://127.0.0.1:27017";

function getExtraRunsConcededPerTeam(database) {
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, conn) {
            if (err) {
                console.log(err.message);
                reject(err);
            } else {

                let ipldb = conn.db(database);
                let matchesCol = ipldb.collection("matchesCol");
                matchesCol.aggregate(
                    [{
                            "$match": {
                                season: 2016
                            }
                        },
                        {
                            "$lookup": {
                                from: "deliveriesCol",
                                localField: "id",
                                foreignField: "match_id",
                                as: "id"
                            }
                        },
                        {
                            "$unwind": "$id"
                        },
                        {
                            "$group": {
                                "_id": "$id.bowling_team",
                                extraRuns: {
                                    $sum: "$id.extra_runs"
                                }
                            }
                        },
                        {
                            $project:
                            {
                                _id:"$null",
                                "label":"$_id",
                                "y":"$extraRuns"
                            }
                        }
                    ]).toArray(function (err, data) {
                    if (err) {
                        console.log(err.message);
                        reject(err);
                    } else {
                        // require("fs").writeFile("JSONfiles/quest3.json", JSON.stringify(data, null, 10), () => {
                        //     console.log("file created");
                        // })
                        resolve(data);
                    }
                })
                conn.close();
            }

        })
    })

}

// getExtraRunsConcededPerTeam("ipldb");

module.exports = {
    getExtraRunsConcededPerTeam: getExtraRunsConcededPerTeam
}